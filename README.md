# Hacker News

This project was developed for **Reign**. 

# File Structure

The project is structured in the following shape:

 1. Index.js file. The entry point for the app.
 2. App.js file: This file contains the main structure of the project and manage when the nested views are rendered.
 3. Pages folder: Contains a folder per app view. In this case, home and favs.
 4. Components folder: This folder has the components used in multiple views.
 5. Assets folder: Contains images used in the app.
 6. Api folder: Requests used in the app.

## How to run

    yarn install
    yarn start

Follow me on Twitter: [@jchavez_l](https://twitter.com/jchavez_l)