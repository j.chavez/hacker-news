import React from 'react';
import moment from 'moment';
import {NewsProps} from '../../interfaces';
import {ReactComponent as Heart} from '../../assets/icons/heart.svg';
import {ReactComponent as HeartOutline} from '../../assets/icons/heart-outline.svg';
import {ReactComponent as Time} from '../../assets/icons/time.svg';
import styles from './News.module.css';

interface Props {
  article: NewsProps,
  favs: NewsProps[],
  updateFavs: (news: NewsProps) => void,
};

const News: React.FC<Props> = ({article, favs, updateFavs}) => {
  const {
    created_at,
    author,
    story_title,
    story_url,
  } = article;

  // Verify if the article is liked
  const isLiked = favs.findIndex(el => el.objectID === article.objectID) > -1;

  return (
    <div className={styles.news}>
      <a href={story_url} target="_blank" rel="noopener noreferrer" className={styles.info}>
        <div className={styles.timeWrap}>
          <Time />
          <span>
            {`${moment(created_at).startOf('hour').fromNow()} by ${author}`}
          </span>
        </div>
        <span className={styles.title}>
          {story_title || 'Unknown title'}
        </span>
      </a>
      <div onClick={() => updateFavs(article)} className={styles.heartWrap}>
        {isLiked ? (
          <Heart className={styles.heart} />
        ) : (
          <HeartOutline className={styles.heart} />
        )}
      </div>
    </div>
  );
};

export default News;
