import axios from 'axios';

export const getNews = async (query: string, page: number) => {
  const baseURL = `https://hn.algolia.com/api/v1/search_by_date?query=${query}&page=${page}`;
  const data = await axios.get(baseURL);
  return data;
};
