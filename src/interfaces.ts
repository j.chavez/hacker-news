//General interface for news
export interface NewsProps {
  author: string,
  comment_text: string,
  created_at: string,
  created_at_i: string,
  num_comments: number | null,
  objectID: string,
  parent_id: number,
  points: number | null,
  story_id: number,
  story_text: string | null,
  story_title: string,
  story_url: string,
  title: string | null,
  url: string | null,
};
