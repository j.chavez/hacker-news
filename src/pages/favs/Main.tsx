import React, {useState, useEffect} from 'react';
import produce from 'immer';
import {NewsProps} from '../../interfaces';
import FavsUI from './components/FavsUI';

interface Props {
  favs: NewsProps[],
  updateFavs: (news: NewsProps) => void,
};

const Home: React.FC<Props> = ({favs, updateFavs}) => {
  const [page, handlePage] = useState<number>(0);
  const [results, handleResults] = useState<NewsProps[]>([]);

  useEffect(() => {
    // Update the results if the favs changes and when the component is mounted
    const base = 20 * page;
    const newResults = favs.slice(base, 20 + base);;
    handleResults(newResults);
  }, [favs]);

  const changePage = (selectedItem: {selected: number}) => {
    // Functions to change page
    const base = 20 * selectedItem.selected;
    const newResults = favs.slice(base, 20 + base);
    handlePage(selectedItem.selected);
    handleResults(newResults);
  };

  return (
    <FavsUI
      favs={favs}
      updateFavs={updateFavs}
      page={page}
      results={results}
      changePage={changePage}
    />
  );
};

export default Home;
