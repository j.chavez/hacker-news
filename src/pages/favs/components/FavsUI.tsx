import React from 'react';
import ReactPaginate from 'react-paginate';
import {NewsProps} from '../../../interfaces';
import News from '../../../components/news/News';
import styles from '../styles/FavsUI.module.css';

interface Props {
  favs: NewsProps[],
  results: NewsProps[],
  updateFavs: (news: NewsProps) => void,
  page: number,
  changePage: (selectedItem: {selected: number}) => void,
};

const FavsUI: React.FC<Props> = props => {
  const {
    favs,
    updateFavs,
    page,
    results,
    changePage,
  } = props;

  return (
    <div className={styles.favs}>
      <div className={styles.news}>
        {results.map(article => (
          <News
            key={article.objectID}
            article={article}
            favs={favs}
            updateFavs={updateFavs}
          />
        ))}
      </div>
      {(favs.length > 0) && (
        <div className={styles.pagination}>
          <ReactPaginate
            breakLabel="..."
            nextLabel=">"
            onPageChange={changePage}
            pageRangeDisplayed={5}
            pageCount={favs.length / 20}
            previousLabel="<"
            className={styles.paginationWrap}
            pageClassName={styles.paginationNumber}
            previousClassName={styles.paginationNumber}
            nextClassName={styles.paginationNumber}
            forcePage={page}
            activeClassName={styles.active}
            breakClassName={styles.paginationNumber}
          />
        </div>
      )}
    </div>
  );
};

export default FavsUI;
