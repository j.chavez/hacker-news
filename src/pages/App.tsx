import React, {useState, useEffect} from 'react';
import cx from 'classnames';
import produce from 'immer';
import {NewsProps} from '../interfaces';
import Logo from '../assets/logo.png';
import Home from './home/Main';
import Favs from './favs/Main';
import styles from './App.module.css';

const App: React.FC = () => {
  // Variable for the type of articles we're going to get
  const [mode, handleMode] = useState<string>('home');
  // Array of favorite articles 
  const [favs, handleFavs] = useState<NewsProps[]>([]);

  useEffect(() => {
    // Getting the fav articles from local storage
    const myFavs = localStorage.getItem('news-favs');
    if (myFavs) {
      handleFavs(JSON.parse(myFavs));
    }
  }, []);

  const updateFavs = (news: NewsProps) => {
    // Verify if the article is in the array and then, saves the new array in local storage
    const index = favs.findIndex(el => el.objectID === news.objectID);
    if (index > -1) {
      const myFavs = produce(favs, draftState => {
        draftState.splice(index, 1);
      });
      handleFavs(myFavs);
      localStorage.setItem('news-favs', JSON.stringify(myFavs));
    } else {
      const newFavs = produce(favs, draftState => {
        draftState.push(news);
      });
      handleFavs(newFavs);
      localStorage.setItem('news-favs', JSON.stringify(newFavs));
    }
  };

  return (
    <div className={styles.app}>
      <div className={styles.header}>
        <img src={Logo} className={styles.logo} alt="" />
      </div>
      <div className={styles.body}>
        <div className={styles.options}>
          <button
            onClick={() => handleMode('home')}
            className={cx({[styles.active]: mode === 'home'})}>
            All
          </button>
          <button
            onClick={() => handleMode('favs')}
            className={cx({[styles.active]: mode === 'favs'})}>
            My Faves
          </button>
        </div>
        <div className={styles.content}>
          {mode === 'home' ? (
            <Home favs={favs} updateFavs={updateFavs} />
          ) : (
            <Favs favs={favs} updateFavs={updateFavs} />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
