import React, {useState, useEffect, useCallback} from 'react';
import {NewsProps} from '../../interfaces';
import HomeUI from './components/HomeUI';
import {getNews} from '../../api/Request';

interface Props {
  favs: NewsProps[],
  updateFavs: (news: NewsProps) => void,
};

const Home: React.FC<Props> = ({favs, updateFavs}) => {
  // Variable for the dropdown, to verify if it's open
  const [open, handleOpen] = useState<boolean>(false);
  const [loading, handleLoading] = useState<boolean>(true);
  const [page, handlePage] = useState<number>(0);
  const [numberPages, handleNumberPages] = useState<number>(0);
  const [news, handleNews] = useState<NewsProps[]>([]);
  const [selectedOption, setSelectedOption] = useState<string>('angular');

  const getNewsType = useCallback((type?: string) => {
    // We built the query for the request
    const newType = type || selectedOption;
    if (newType !== 'angular') {
      return `${newType}js`;
    }
    return newType;
  }, [selectedOption]);

  const initialFetch = useCallback(async (type?: string, newPage?: number) => {
    const response = await getNews(getNewsType(type), newPage || page);
    if (response.data) {
      handleNews(response.data.hits);
      handleNumberPages(response.data.nbPages);
    } else {
      alert('Try again later');
    }
    handleLoading(false);
  }, []);

  useEffect(() => {
    // Getting the articles type from local storage
    const previous = localStorage.getItem('news-type');
    if (!previous) {
      localStorage.setItem('news-type', selectedOption);
    } else {
      setSelectedOption(previous);
    }
    initialFetch();
  }, []);

  // Open or close the dropdown
  const toggling = () => handleOpen(!open);

  const onOptionClicked = (value: string) => {
    // Resets the values when the articles type was changed
    setSelectedOption(value);
    localStorage.setItem('news-type', value);
    handleOpen(false);
    handlePage(0);
    handleLoading(true);
    handleNews([]);
    initialFetch(value);
  };

  const queryNewPage = (selectedItem: { selected: number; }) => {
    // Function to update the current page
    handlePage(selectedItem.selected);
    handleLoading(true);
    initialFetch(selectedOption, selectedItem.selected );
  };

  return (
    <HomeUI
      loading={loading}
      isOpen={open}
      selectedOption={selectedOption}
      toggling={toggling}
      onOptionClicked={onOptionClicked}
      news={news}
      numberPages={numberPages}
      page={page}
      queryNewPage={queryNewPage}
      favs={favs}
      updateFavs={updateFavs}
    />
  );
};

export default Home;
