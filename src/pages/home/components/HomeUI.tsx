import React from 'react';
import ReactPaginate from 'react-paginate';
import {ReactComponent as Arrow} from '../../../assets/icons/chevron-down.svg';
import {NewsProps} from '../../../interfaces';
import News from '../../../components/news/News';
import Loader from '../../../components/loader/Loader';
import Angular from '../../../assets/logos/angular.png';
import ReactLogo from '../../../assets/logos/react.png';
import Vue from '../../../assets/logos/vue.png';
import styles from '../styles/HomeUI.module.css';

interface Props {
  loading: boolean,
  isOpen: boolean,
  selectedOption: string,
  toggling: () => void,
  onOptionClicked: (value: string) => void,
  news: NewsProps[],
  numberPages: number,
  page: number,
  queryNewPage: (selectedItem: {selected: number}) => void,
  favs: NewsProps[],
  updateFavs: (news: NewsProps) => void,
};

const HomeUI: React.FC<Props> = props => {
  const {
    isOpen,
    selectedOption,
    toggling,
    onOptionClicked,
    loading,
    news,
    numberPages,
    queryNewPage,
    page,
    favs,
    updateFavs,
  } = props;

  return (
    <div className={styles.home}>
      <div className={styles.selector}>
        <div className={styles.selectorHeader} onClick={toggling}>
          <span>{selectedOption}</span>
          <Arrow className={styles.arrow} />
        </div>
        {isOpen && (
          <div className={styles.selectorListContainer}>
            <div className={styles.selectorList}>
              <div className={styles.selectorListItem} onClick={() => onOptionClicked('angular')}>
                <img src={Angular} alt="" />
                <span>Angular</span>
              </div>
              <div className={styles.selectorListItem} onClick={() => onOptionClicked('react')}>
                <img src={ReactLogo} alt="" />
                <span>React</span>
              </div>
              <div className={styles.selectorListItem} onClick={() => onOptionClicked('vue')}>
                <img src={Vue} alt="" />
                <span>Vue</span>
              </div>
            </div>
          </div>
        )}
      </div>
      {loading ? (
        <div className={styles.loader}>
          <Loader />
        </div>
      ) : (
        <>
          <div className={styles.news}>
            {news.map(article => (
              (article.author && article.created_at && article.story_title && article.story_url) && (
                <News
                  key={article.objectID}
                  article={article}
                  favs={favs}
                  updateFavs={updateFavs}
                />
              )
            ))}
          </div>
          <div className={styles.pagination}>
            <ReactPaginate
              breakLabel="..."
              nextLabel=">"
              onPageChange={queryNewPage}
              pageRangeDisplayed={5}
              pageCount={numberPages}
              previousLabel="<"
              className={styles.paginationWrap}
              pageClassName={styles.paginationNumber}
              previousClassName={styles.paginationNumber}
              nextClassName={styles.paginationNumber}
              forcePage={page}
              activeClassName={styles.active}
              breakClassName={styles.paginationNumber}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default HomeUI;
